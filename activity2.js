// 2. Create new collection of users
db.users.insertMany([
    {"firstName":"Stephen", "lastName":"Hawking", "age":76.0, "email":"stephenhawking@mail.com", "department":"HR"},
    {"firstName":"Neil", "lastName":"Armstrong", "age":82.0, "email":"neilarmstrong@mail.com", "department":"HR"},
    {"firstName":"Bill", "lastName":"Gates", "age":65.0, "email":"billgates@mail.com", "department":"Opetations"},
    {"firstName":"Jane", "lastName":"Doe", "age":21, "email":"janedoe@mail.com", "department":"HR"}
])

// 3. Or and regex
db.users.find({$or:[
    { firstName: { $regex: /s/i } },
    { lastName: { $regex: /d/i } }
]}, 
    {"firstName":1, "lastName":1, "_id":0}
)


// 4. gte
//db.users.find({"department": "HR", "age": {$gte: 70}})
db.users.find({$and:[{"department": "HR"}, {"age": {$gte: 70}}]})


// 5.
//db.users.find({firstName: { $regex: /e/i }, age: {$lte:30}})
db.users.find({$and:[
    {firstName: { $regex: /e/i }}, 
    {age: {$lte:30}}
    ]
})